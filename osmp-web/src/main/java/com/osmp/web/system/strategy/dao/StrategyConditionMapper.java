package com.osmp.web.system.strategy.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.strategy.entity.StrategyCondition;

public interface StrategyConditionMapper extends BaseMapper<StrategyCondition> {

}
